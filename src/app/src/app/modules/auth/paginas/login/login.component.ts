import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(
    private matIconPerzonalizado : MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconPerzonalizado.addSvgIcon(
      'icon_facebook',
      this.domSanitizer.bypassSecurityTrustResourceUrl("assets/img/facebook.png")
    )
    this.matIconPerzonalizado.addSvgIcon(
      'icon_google',
      this.domSanitizer.bypassSecurityTrustResourceUrl("assets/img/google.png")
    )
  }
}
